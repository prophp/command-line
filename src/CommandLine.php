<?php

namespace ProPhp\CommandLine;

class CommandLine
{
    private /*array*/ $arguments = [];

    private /*array*/ $options = [];

    public function __construct(array $argv)
    {
        foreach (array_slice($argv, 1) as $parameter) {
            if ($this->isOption($parameter)) {
                list($optionTitle, $optionValue) = array_pad(explode("=", substr($parameter, 2)), 2, null);
                $optionValue = $optionValue === null ? null : trim($optionValue);
                if (in_array($optionValue, ['true', 'false'], true)) {
                    $optionValue = filter_var($optionValue, FILTER_VALIDATE_BOOLEAN);
                }
                $this->options[trim($optionTitle)] = $optionValue;
                continue;
            }

            if ($this->isArgument($parameter)) {
                $this->arguments[] = $parameter;
            }
        }
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function getArgument(int $position): ?string
    {
        return $this->arguments[$position] ?? null;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function optionIsSet(string $optionTitle): bool
    {
        return array_key_exists($optionTitle, $this->options);
    }

    public function getOptionValue(string $optionTitle): ?string
    {
        return $this->options[$optionTitle] ?? null;
    }

    public static function stringToArgv(string $string): array
    {
        exec(dirname(__DIR__) . "/bin/exec return-argv-json $string", $output);

        return json_decode($output[0], true);
    }

    private function isArgument(string $string): bool
    {
        return substr($string, 0, 2) !== "--";
    }

    private function isOption(string $string): bool
    {
        return substr($string, 0, 2) === "--";
    }
}